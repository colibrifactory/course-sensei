<!DOCTYPE html>
<html lang="en"> 
  <head>
    <meta charset="utf-8" />
    <title>Course Sensei</title>
    <meta name="description" content="Course SENSEI is a web application to handle your courses and student's qualifications" />    
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
  </head>
  <body>
    <div id="back1">
      <div id="wrapper1">
        <img src="images/logo.png">
      </div>
    </div>
  	<div id="back2">
  		<div id="wrapper2">
		  <?php
	          if(!isset($_GET["action"])){
	            include("courses/index.php");
	          }
	          elseif($_GET["action"] == "new"){
	            include("courses/new.php");
	          }
	          elseif($_GET["action"] == "edit"){
	            include("courses/edit.php");
	          }
            elseif($_GET["action"] == "show"){
              include("courses/show.php");
            }
	      ?>
    	</div>
      <div class="clear"></div>
    </div>
  </body>
 </html>  