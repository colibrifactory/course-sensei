<!DOCTYPE html>
<html lang="en"> 
  <head>
    <meta charset="utf-8" />
    <title>Course Sensei</title>
    <meta name="description" content="Course SENSEI is a web application to handle your courses and student's qualifications" />    
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
  </head>
  <body>
  	<div id="wrapper">
  		<div id="content">
		  <?php
	          if(!isset($_GET["action"])){
	            include("users/new.php");
	          }
	          elseif($_GET["action"] == "success"){
	            include("users/success.php");
	          }
	          elseif($_GET["action"] == "edit"){
	            include("courses/edit.php");
	          }
	      ?>
    	</div>
    </div>
  </body>
 </html>  