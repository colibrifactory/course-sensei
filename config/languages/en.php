<?php

   // COURSES: translations for index.php
   define("INDEX_COURSES_TITLE", "My Courses");
   define("COURSE", "COURSE");
   define("SCHEDULE", "SCHEDULE");
   define("START_DATE", "START DATE");
   define("END_DATE", "END DATE");

   // COURSES: translations for new.php
   define("NEW_COURSES_TEXT", "Complete the form to create a new course");
   define("NEW_COURSE", "COURSE:");
   define("NEW_SCHEDULE", "SCHEDULE:");
   define("NEW_START_DATE", "START DATE:");
   define("NEW_END_DATE", "END DATE:");

   // COURSES: translations for show.php
   define("SHOW_COURSES_TITLE", "General information:");
   define("SHOW_STUDENTS_TITLE", "Students in course:");
   define("STUDENT", "STUDENT");

   // COURSES: translations for edit.php
   define("EDIT_COURSES_TEXT", "Complete the form to edit a course");
   define("EDIT_COURSE", "COURSE:");
   define("EDIT_SCHEDULE", "SCHEDULE:");
   define("EDIT_START_DATE", "START DATE:");
   define("EDIT_END_DATE", "END DATE:");

   // ADD, DELETE, EDIT, SHOW and BACK links
   define("ADD", "ADD NEW"); 
   define("CREATE", "CREATE");
   define("EDIT", "EDIT"); 
   define("SHOW", "SHOW"); 
   define("DELETE", "DELETE"); 
   define("BACK", "BACK TO MAIN MENU");

   // SIGN_IN: translations for users/new.php
   define("NEW_USERS_TEXT", "Complete for sign in");
   define("NEW_USER_NAME", "Name: ");
   define("NEW_USER_LASTNAME1", "Lastname: ");
   define("NEW_USER_LASTNAME2", "Lastname: ");
   define("NEW_USER_EMAIL", "Email: ");

?>
