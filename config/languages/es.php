<?php

   // CURSOS: traducciones para index.php
   define("INDEX_COURSES_TITLE", "Mis cursos");
   define("COURSE", "CURSO");
   define("SCHEDULE", "HORARIO");
   define("START_DATE", "INICIA");
   define("END_DATE", "TERMINA");

   // CURSOS: traducciones para new.php
   define("NEW_COURSES_TEXT", "Completa el formulario para agregar un nuevo curso");
   define("NEW_COURSE", "CURSO:");
   define("NEW_SCHEDULE", "HORARIO:");
   define("NEW_START_DATE", "INICIA:");
   define("NEW_END_DATE", "TERMINA:");

   // CURSOS: traducciones para show.php
   define("SHOW_COURSES_TITLE", "Informaci&oacute;n general:");
   define("SHOW_STUDENTS_TITLE", "Estudiantes inscritos:");
   define("STUDENT", "ESTUDIANTE");

   // CURSOS: traducciones para edit.php
   define("EDIT_COURSES_TEXT", "Completa el formulario para actualizar tu curso");
   define("EDIT_COURSE", "CURSO:");
   define("EDIT_SCHEDULE", "HORARIO:");
   define("EDIT_START_DATE", "INICIA:");
   define("EDIT_END_DATE", "TERMINA:");

   // Enlaces de AGREGAR, EDITAR, VER, ELIMINAR y REGRESAR
   define("ADD","AGREGAR");
   define("CREATE","CREAR");
   define("EDIT","EDITAR");
   define("SHOW", "VER");
   define("DELETE","ELIMINAR");
   define("BACK","REGRESAR A MENU");

   // USUARIOS: traducciones para new.php
   define("NEW_USERS_TEXT", "Completa el formulario para incribirte en un curso");
   define("NEW_USER_NAME", "Nombre: ");
   define("NEW_USER_LASTNAME1", "Apellido paterno: ");
   define("NEW_USER_LASTNAME2", "Apellido materno: ");
   define("NEW_USER_EMAIL", "EMAIL: ");

?>
