<?php

  include("config/dbconnection.php");

  // Inserting course in database
  $query = "SELECT * FROM courses;";

  mysqli_query($link, $query);


 if ($result = mysqli_query($link, $query, MYSQLI_USE_RESULT)) {
         // Printing courses registers in website
         
         print("<select name=\"course_id\">");
         while ($row = mysqli_fetch_row($result)) {
            printf("<option value=\"%u\">%s</option>", $row[4], $row[0]);
         }
         print("</select>");
         mysqli_free_result($result);
  }

  // Closing connection
  mysqli_close($link);


?>
